import ssl
import numpy
import random
import string
import nltk
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

"""
try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    pass
else:
    ssl._create_default_https_context = _create_unverified_https_context
# First time use only
nltk.download('punkt')
nltk.download('wordnet')
"""

f = open('chatbot.txt', 'r', errors = 'ignore') # Open the corpus
raw = f.read()
raw = raw.lower() # Converts to lowercase
f.close()

sent_tokens = nltk.sent_tokenize(raw) # Converts to list of sentences
word_tokens = nltk.word_tokenize(raw) # Converts to list of words

# Pre-processing the raw text
lemmer = nltk.stem.WordNetLemmatizer() # WordNet is a semantically-oriented dictionary of English included in NLTK.

# Remove punctuation
remove_punct_dict = dict((ord(punct), None) for punct in string.punctuation)

# Keyword matching
GREETING_INPUT = ("hello", "hi", "greetings", "sup", "what's up", "hey")
GREETING_RESPONSE = ["hi", "hey", "*nods*", "hi there", "hello", "I am glad! You are talking to me"]

"""
Take tokens as input and return normalized tokens
"""
def LemTokens(tokens):
    return [lemmer.lemmatize(token) for token in tokens]

"""
Take a text as input and return normalized lemmaswh
"""
def LemNormalize(text):
    return LemTokens(nltk.word_tokenize(text.lower().translate(remove_punct_dict)))

def greeting(sentence):
    for word in sentence.split():
        if word.lower() in GREETING_INPUT:
            return random.choice(GREETING_RESPONSE)

def response(user_response):
    robo_response = ""
    TfidVec = TfidfVectorizer(tokenizer=LemNormalize, stop_words="english")
    tfidf = TfidVec.fit_transform(sent_tokens)
    vals = cosine_similarity(tfidf[-1], tfidf) # Compute the similarity between the last sent_token and the whole tokens set.
    idx = vals.argsort()[0][-2] # Sort the similarities between the last sent_token and the whole tokens set, then get the index of the highest one.
    flat = vals.flatten()
    flat.sort()
    req_tfidf = flat[-2] # Get the tfidf of the user input

    if(req_tfidf == 0):
        robo_response = robo_response + "I am sorry! I don't understand you"
        return robo_response
    else:
        robo_response = robo_response + sent_tokens[idx]
        return robo_response


flag = True
print("ROBO: My name is Robo. I will answer your queries about Chatbots. If you want to exit, type 'Bye' !")

while (flag == True):
    user_response = input()
    user_response = user_response.lower()
    if (user_response != 'bye'):
        if (user_response == "thanks" or user_response == "thank you"):
            flag = False
            print("ROBO: You are welcome..")
        else:
            if (greeting(user_response) != None):
                print("ROBO: " + greeting(user_response))
            else:
                sent_tokens.append(user_response)
                word_tokens = word_tokens + nltk.word_tokenize(user_response)
                final_words = list(set(word_tokens))
                print("ROBO: ", end="")
                print(response(user_response))
                sent_tokens.remove(user_response)
    else:
        flag = False
        print("ROBO: Bye! Take care..")