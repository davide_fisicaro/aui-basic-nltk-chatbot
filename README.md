# README #
Basic example of the usage of nltk python lybrary to build an IR-based chatbot talking about chatbots.

## Usage ##
Install Python 3.7 from the [official page](https://www.python.org).  
From terminal run:
``` 
pip install -r requirements.txt
````
then:
```
python chatbot.py
```
To see all the available collections, corpora and models from the nltk GUI, run from terminal:
```
python download.py
```  
  

<hr>

*Created by Davide Fisicaro, September 27th, 2018*<br>
*davide.fisicaro@polimi.it*