"""
Created by Davide Fisicaro

Open the nltk GUI to download collections, corpora and models.
"""

import nltk
import ssl

try:
    print("Attempting to establish SSL connection ...")
    _create_unverified_https_context = ssl._create_unverified_context
    print("... SSL connection created")
except AttributeError:
    print("SSL connection refused ... falling back to default protocol")
else:
    ssl._create_default_https_context = _create_unverified_https_context

nltk.download()